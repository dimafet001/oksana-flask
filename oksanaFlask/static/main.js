$(document).ready(function() {

	$(".fp-controlArrow").css({display: "none"});
	$(".slide-change-btn").on("click", function() {
		// $(".fp-controlArrow.fp-next").css({display: "inline"});
		$(".fp-controlArrow.fp-next").trigger("click");
	});

	// to extend text area when it overflows lines
	$("textarea").keyup(function(e) {
	    while($(this).outerHeight() < this.scrollHeight + parseFloat($(this).css("borderTopWidth")) + parseFloat($(this).css("borderBottomWidth"))) {
	        $(this).height($(this).height()+1);
	        $(".send-div").height($(".send-div").height()+1);
    	}
    });

	// slide up to the top
    $("div#btn-up").click(function() {
    	//TODO: does not work in safari
    	$("html, body").animate({ scrollTop: 0 }, "slow");
  		return false;
    });

	$('.rslides_tabs > li').on("click", function(e){
		$(this).children("a").trigger("click");
		e.stopPropagation();
		return false;
	});

	//TODO: WHAT IS IT?
	document.getElementById("input-email").setCustomValidity("enter your email");


	var isErrorOut = false;
	var isErrorContactsOut = false;
	var justWasPressed = false;


	$("button[type='submit']").click(function(e) {
		e.preventDefault();
		if (justWasPressed) return;		

		var name = $("#input-name").val();
		var email = $("#input-email").val();
		var tel = $("#input-tel").val();
		var msg = $("#input-msg").val();

		var str = "name=" + name + "&email=" + email
				+ "&tel=" + tel + "&msg=" + msg;

		if (!(msg && name && (email || tel))) {
			if (!(msg && name || isErrorOut)) {
				document.getElementById("error").style.bottom = "50px";
				isErrorOut = true;
				setTimeout(function() {
					document.getElementById("error").style.bottom = "-90px";
					isErrorOut = false;
				}, 4000);
			} else if (!(email || tel || isErrorContactsOut)) {
				document.getElementById("error-contacts").style.bottom = "50px";
				isErrorContactsOut = true;
				setTimeout(function() {
					document.getElementById("error-contacts").style.bottom = "-90px";
					isErrorContactsOut = false;
				}, 4000);
			}
			return false;
		}

		$.ajax({
			type: "POST",
			url: "send",
			data: str,
			success: function() {
				justWasPressed = true;
				document.getElementById("success-send").style.bottom = "60px";
				setTimeout(function() {
					document.getElementById("success-send").style.bottom = "-90px";
					justWasPressed = false;
				}, 15000);
			},
			error: function(request, error) {
				console.log(request + "\n" + error);
				document.getElementById("error-send").style.bottom = "60px";
				setTimeout(function() {
					document.getElementById("error-send").style.bottom = "-90px";
				}, 5000);
			}
		});

		
	});
});
