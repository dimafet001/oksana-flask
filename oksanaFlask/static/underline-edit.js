function setUnderlines() {
	window.sr = ScrollReveal();
	sr.reveal('.title-underline', {
		afterReveal: function() {
			var title = document.getElementsByClassName("title")[0];
			var width = title.offsetWidth * 0.36;
			var underline = document.getElementsByClassName("title-underline")[0];
			$(".title-underline").animate({
				width : width + "px"			
			}, 200);
		}
	});
}  

$(document).ready($(window).scroll(setUnderlines()));


