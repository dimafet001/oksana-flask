$(document).ready(function(){

	var $tabs = $(".rslides_tabs");
	$tabs.css("width", String(parseFloat($tabs.css("width")) - parseFloat($(".rslides_nav").css("width")) * 2));
	$(".rslides_tabs > li").children("a").append("<div class='rslides_selector'></div>");

	$(window).resize(function(){
		// timeout нужен, потому что resize не успевает за css
		setTimeout(function() {
			$tabs.css("width", String(parseFloat($(".wrapper").css("width"))) - parseFloat($(".rslides_nav").css("width")) * 2);
		}, 50);
		});
});