#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import Flask, render_template, request, current_app
import os

app = Flask(__name__)
app.config.update(dict(
    DATABASE=os.path.join(app.root_path, 'messages.db'),
    #SECRET_KEY='development key',
    #USERNAME='admin',
    #PASSWORD='default'
))
app.config.from_envvar('OKSANAFLASK_SETTINGS', silent=True)
""" DATABASE PART """

import sqlite3
from flask import g


def get_db():
    db = getattr(g, 'db', None)
    if db is None:
        db = g.db = sqlite3.connect(current_app.config['DATABASE'])
        g.db.row_factory = sqlite3.Row
    return db

@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, 'db', None)
    if db is not None:
        db.close()

#-------------------



timetable = ["11:00-13:00", "", "11:00-13:00", "", "15:00-17:00", "15:00-17:00", "17:00-19:00"]
tel = "8-918-560-88-44"
email = "oksana-rostov80@mail.ru"
addresses = [("2-я краснодарская, 84/2", [47.205190, 39.626502])]
sliderimages = ["main1.jpg", "main2.jpg", "main3.jpg", "main4.jpg"]



@app.route('/', methods=['GET', 'POST'])
def face():
    return render_template('index.html', 
        timetable=timetable, tel=tel, email=email, addresses=addresses, sliderimages=sliderimages)

@app.route('/eng')
def eng():
    return render_template('eng.html', 
        timetable=timetable, tel=tel, email=email, addresses=addresses, sliderimages=sliderimages)


@app.route('/credits')
def credits():
    return render_template('credits.html')


@app.route('/send', methods=['POST'])
def send():
    if request.method == 'POST':
        #do all the work with saving to db
        name = request.form.get('name')
        email = request.form.get('email')
        tel = request.form.get('tel')
        msg = request.form.get('msg')
        
        # save to db now
        cur = get_db().cursor()
        #cur.execute('''CREATE TABLE msg
            # (name text, email text, tel text, msg text)''')
        cur.execute('INSERT INTO msg VALUES ("{}", "{}", "{}", "{}")'.format(name, email, tel, msg))
        get_db().commit();
        cur.close()
        return "Successful";


if __name__ == "__main__":
   app.run()
